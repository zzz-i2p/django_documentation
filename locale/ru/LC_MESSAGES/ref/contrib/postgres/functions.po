# Russian translations for Django package.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# Automatically generated, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: Django 1.9\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-11-16 11:56+0200\n"
"PO-Revision-Date: 2015-11-17 10:48+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../../ref/contrib/postgres/functions.txt:3
msgid "PostgreSQL specific database functions"
msgstr ""

#: ../../ref/contrib/postgres/functions.txt:5
msgid ""
"All of these functions are available from the ``django.contrib.postgres."
"functions`` module."
msgstr ""

#: ../../ref/contrib/postgres/functions.txt:11
msgid "``RandomUUID``"
msgstr ""

#: ../../ref/contrib/postgres/functions.txt:17
msgid "Returns a version 4 UUID."
msgstr ""

#: ../../ref/contrib/postgres/functions.txt:19
msgid "Requires PostgreSQL 9.4 or greater."
msgstr ""

#: ../../ref/contrib/postgres/functions.txt:21
msgid ""
"The `pgcrypto extension`_ must be installed. You can use the :class:`~django."
"contrib.postgres.operations.CryptoExtension` migration operation to install "
"it."
msgstr ""

#: ../../ref/contrib/postgres/functions.txt:27
#: ../../ref/contrib/postgres/functions.txt:48
msgid "Usage example::"
msgstr ""

#: ../../ref/contrib/postgres/functions.txt:33
msgid "``TransactionNow``"
msgstr ""

#: ../../ref/contrib/postgres/functions.txt:37
msgid ""
"Returns the date and time on the database server that the current "
"transaction started. If you are not in a transaction it will return the date "
"and time of the current statement. This is a complement to :class:`django.db."
"models.functions.Now`, which returns the date and time of the current "
"statement."
msgstr ""

#: ../../ref/contrib/postgres/functions.txt:43
msgid ""
"Note that only the outermost call to :func:`~django.db.transaction.atomic()` "
"sets up a transaction and thus sets the time that ``TransactionNow()`` will "
"return; nested calls create savepoints which do not affect the transaction "
"time."
msgstr ""
